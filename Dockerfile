from registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="linapple"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"


RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --force-yes \
git libcurl4-openssl-dev zlib1g-dev libzip-dev libsdl-image1.2-dev

run git clone https://github.com/dabonetn/linapple-pie.git
add Makefile.patch /

workdir linapple-pie
run git apply /Makefile.patch
workdir src

run make
run make install

add metadata /metadata
